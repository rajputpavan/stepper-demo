import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({});

class StepTwoSecond extends React.Component {
   render(){
       return (<div>
             <Paper>
                <h1>SPECIFICATION content from StepTwoSecond Component</h1>
             </Paper>
       </div>)
   }

}

StepTwoSecond.propTypes = {
    classes: PropTypes.object.isRequired,
  };
export default withStyles(styles)(StepTwoSecond);
