import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({});

class StepThreeSecond extends React.Component {
   render(){
       return (<div>
             <Paper>
             <h1>ENGINE & TRANSMISSION content from StepThreeSecond Component</h1>
             </Paper>
       </div>)
   }

}

StepThreeSecond.propTypes = {
    classes: PropTypes.object.isRequired,
  };
export default withStyles(styles)(StepThreeSecond);
