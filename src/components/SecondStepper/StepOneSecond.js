import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({});

class StepOneSecond extends React.Component {
   render(){
       return (<div>
             <Paper>
                    <h1>DETAILS content from StepOeSecond Component</h1>
             </Paper>
       </div>)
   }

}

StepOneSecond.propTypes = {
    classes: PropTypes.object.isRequired,
  };
export default withStyles(styles)(StepOneSecond);
