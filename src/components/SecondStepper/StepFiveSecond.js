import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({});

class StepFiveSecond extends React.Component {
   render(){
       return (<div>
             <Paper>
             <h1>FLUIDS content from StepFiveSecond Component</h1>
             </Paper>
       </div>)
   }

}

StepFiveSecond.propTypes = {
    classes: PropTypes.object.isRequired,
  };
export default withStyles(styles)(StepFiveSecond);
