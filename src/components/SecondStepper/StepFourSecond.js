import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({});

class StepFourSecond extends React.Component {
   render(){
       return (<div>
             <Paper>
                <h1>WHEELS & TYRES content from StepFourSecond Component</h1>
             </Paper>
       </div>)
   }

}

StepFourSecond.propTypes = {
    classes: PropTypes.object.isRequired,
  };
export default withStyles(styles)(StepFourSecond);
