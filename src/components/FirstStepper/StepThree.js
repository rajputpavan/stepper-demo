import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";

const styles = theme => ({});

class StepThree extends React.Component {
  render() {
    return (
      <div>
        <Paper>
          <h1>ADD ONS content loading from StepThree component</h1>
        </Paper>
      </div>
    );
  }
}

StepThree.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(StepThree);
