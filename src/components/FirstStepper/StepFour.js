import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";

const styles = theme => ({});

class StepFour extends React.Component {
  render() {
    return (
      <div>
        <Paper>
          <h1>PERSONAL DETAILS & SUMMARY content loading from StepFour component</h1>
        </Paper>
      </div>
    );
  }
}

StepFour.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(StepFour);