import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({});

class StepOne extends React.Component {
   render(){
       return (<div>
             <Paper>
                    <h1>WHEN & WHERE content loading from StepOne component</h1>
             </Paper>
       </div>)
   }

}

StepOne.propTypes = {
    classes: PropTypes.object.isRequired,
  };
export default withStyles(styles)(StepOne);