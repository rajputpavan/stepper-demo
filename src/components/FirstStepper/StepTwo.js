import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";

const styles = theme => ({});

class StepTwo extends React.Component {
  render() {
    return (
      <div>
        <Paper>
          <h1>VEHICLE SELECTION content loading from StepTwo component</h1>
        </Paper>
      </div>
    );
  }
}

StepTwo.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(StepTwo);
