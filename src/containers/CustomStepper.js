import React from "react";
import PropTypes from "prop-types";
import queryString from "query-string";
import { withRouter } from 'react-router-dom'
import { withStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { stepperData } from "../data/mockSteps";
import  StepOne  from "../components/FirstStepper/StepOne";
import  StepTwo  from "../components/FirstStepper/StepTwo";
import  StepThree  from "../components/FirstStepper/StepThree";
import  StepFour  from "../components/FirstStepper/StepFour";
import  StepFive  from "../components/FirstStepper/StepFive";
import  StepOneSecond  from "../components/SecondStepper/StepOneSecond";
import  StepTwoSecond  from "../components/SecondStepper/StepTwoSecond";
import  StepThreeSecond  from "../components/SecondStepper/StepThreeSecond";
import  StepFourSecond  from "../components/SecondStepper/StepFourSecond";
import  StepFiveSecond  from "../components/SecondStepper/StepFiveSecond";

const styles = theme => ({
  root: {
    width: "90%",
    margin: "auto"
  },
  backButton: {
    marginRight: theme.spacing.unit
  },
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit
  }
});

class CustomStepper extends React.Component {
  state = {
    activeStep: 0,
    whichStepper: "",
    steps: []
  };
  async componentDidMount() {
    const params = queryString.parse(this.props.location.search);
    const stepper = params.stepper;
    this.setState({
      steps: stepperData[stepper],
      whichStepper: stepper
    });
  }
  getStepContent = (stepIndex) => {
    const {whichStepper} = this.state;
    if( whichStepper === "stepperOne"){
      switch (stepIndex) {
        case 0:
          return <StepOne/>;
        case 1:
          return <StepTwo/>;
        case 2:
          return <StepThree/>;
        case 3:
          return <StepFour/>;
        case 4:
          return <StepFive/>;
        default:
          return 'Unknown stepIndex';
      }
    }
    if(whichStepper === "stepperTwo"){
      switch (stepIndex) {
        case 0:
          return <StepOneSecond/>;
        case 1:
          return <StepTwoSecond/>;
        case 2:
          return <StepThreeSecond/>;
        case 3:
          return <StepFourSecond/>;
        case 4:
          return <StepFiveSecond/>;
        default:
          return 'Unknown stepIndex';
      }
    }
  }

  handleNext = () => {
    this.setState(state => ({
      activeStep: state.activeStep + 1
    }));
  };

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1
    }));
  };

  handleReset = () => {
    this.setState({
      activeStep: 0
    });
  };

  render() {
    const { classes } = this.props;
    //const {steps} = getSteps();
    const { activeStep, steps } = this.state;

    return (
      <div className={classes.root}>
        <Stepper activeStep={activeStep} alternativeLabel>
          {steps.map(label => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>
        <div>
          {this.state.activeStep === steps.length ? (
            <div>
              <Typography className={classes.instructions}>
                All steps completed
              </Typography>
              <Button onClick={this.handleReset}>Reset</Button>
            </div>
          ) : (
            <div>
              <div>
                {this.getStepContent(activeStep)}
             </div>
              <div>
                <Button
                  disabled={activeStep === 0}
                  onClick={this.handleBack}
                  className={classes.backButton}
                >
                  Back
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleNext}
                >
                  {activeStep === steps.length - 1 ? "Finish" : "Next"}
                </Button>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

CustomStepper.propTypes = {
  classes: PropTypes.object
};

export default withStyles(styles) (withRouter(CustomStepper));
